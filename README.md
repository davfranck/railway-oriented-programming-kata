# Kata Railway oriented programming

Le but de ce kata est d'explorer une autre façon de gérer les erreurs qu'avec les exceptions. 
Les exceptions (notamment) ont le défaut de ne pas rendre visible ce qui se passe mal. 
Expérimentez un refactoring en supprimant les exceptions en les remplaçant par une autre structure. 
Essais possibles : 
* un `Either` de [Vavr](https://www.vavr.io/)
* un objet `Result` maison. 
Avec une erreur flexible (on peut aussi bien mettre une `String` ou une `Exception` ou n'importe quel autre type)
Ou avec une erreur déjà typée `RuntimeException` ? (ce qui peut alléger les signatures). 
* un `Result` d'une lib [comme celle-là](https://github.com/StefanMacke/ao-railway). 

## Doc utile
La conférence initiale de Scott Wlaschin : https://vimeo.com/113707214 référencé dans [son article](https://fsharpforfunandprofit.com/rop/). 

[Un article](https://fsharpforfunandprofit.com/posts/against-railway-oriented-programming/) où il décrit les cas où il ne faut probablement pas utiliser ce style.  

[Un article](https://www.thoughtworks.com/insights/blog/either-data-type-alternative-throwing-exceptions) sur le blog de Thoughtworks. 

[Un exemple](https://www.youtube.com/watch?v=YbuSuSpzee4) d'usage en C#. 

## Amélioration du Kata

* TODO idées