package rop.spi;

import rop.domain.Claim;

public interface ClaimRepository {
    void createClaim(Claim claim);
    String nextClaimId();
}
