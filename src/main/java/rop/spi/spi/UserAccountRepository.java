package rop.spi.spi;

import rop.domain.UserAccount;

public interface UserAccountRepository {
    UserAccount findUserAccount(String username);
}
