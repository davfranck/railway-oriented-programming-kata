package rop.domain;

public enum AccountStatus {
    OPEN, CLOSED
}
