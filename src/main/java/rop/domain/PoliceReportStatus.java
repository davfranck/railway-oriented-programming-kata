package rop.domain;

public enum PoliceReportStatus {
    NO_REPORT,
    REPORTED;
}
