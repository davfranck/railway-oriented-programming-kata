package rop.domain;

public record UserAccount(String username, AccountStatus accountStatus) {
    public boolean isClosed() {
        return AccountStatus.CLOSED.equals(accountStatus);
    }

    public boolean isOpen() {
        return !isClosed();
    }
}
