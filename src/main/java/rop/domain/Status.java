package rop.domain;

public enum Status {
    IN_PROGRESS, REFUSED;
}
