package rop.domain;

public record Claim(String id, Status status, String name, Reason reason, PoliceReportStatus policeReportStatus) {
}
