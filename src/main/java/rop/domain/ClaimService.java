package rop.domain;

import org.springframework.stereotype.Service;
import rop.domain.exception.BusinessException;
import rop.domain.exception.TechnicalException;
import rop.infra.EmailAdapter;
import rop.spi.ClaimRepository;
import rop.spi.spi.UserAccountRepository;

@Service
public class ClaimService {

    private final EmailAdapter emailAdapter;
    private final ClaimRepository claimRepository;
    private final UserAccountRepository userAccountRepository;

    public ClaimService(EmailAdapter emailAdapter, ClaimRepository claimRepository, UserAccountRepository userAccountRepository) {
        this.emailAdapter = emailAdapter;
        this.claimRepository = claimRepository;
        this.userAccountRepository = userAccountRepository;
    }

    public Claim createInsuranceRecord(ClaimCreationCommand command) {
        if (isClaimValid(command)) {
            if (userAccountIsOpen(command.username())) {
                Claim claim = new Claim(claimRepository.nextClaimId(), Status.IN_PROGRESS, command.username(), command.reason(), command.policeReportStatus());
                claimRepository.createClaim(claim);
                try {
                    emailAdapter.sendClaimCreatedEmail(claim);
                } catch (TechnicalException e) {
                    // email pas envoyé, mais ce n'est pas grave ici
                }
                // TODO : faire un traitement qui retourne une objet qui n'est pas un Claim ?
                return claim;
            } else {
                throw new BusinessException("User account is closed");
            }
        } else {
            Claim claim = new Claim(claimRepository.nextClaimId(), Status.REFUSED, command.username(), command.reason(), command.policeReportStatus());
            claimRepository.createClaim(claim);
            return claim;
        }
    }

    private boolean isClaimValid(ClaimCreationCommand claimCreationCommand) {
        return claimCreationCommand.reason() != Reason.BURGLARY || claimCreationCommand.policeReportStatus() == PoliceReportStatus.REPORTED;
    }

    private boolean userAccountIsOpen(String username) {
        UserAccount account = userAccountRepository.findUserAccount(username);
        return account.isOpen();
    }

}
