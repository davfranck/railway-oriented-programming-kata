package rop.domain;

public record ClaimCreationCommand(String username, Reason reason, PoliceReportStatus policeReportStatus) {
}
