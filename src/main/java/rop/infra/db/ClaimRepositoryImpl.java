package rop.infra.db;

import org.springframework.stereotype.Repository;
import rop.domain.Claim;
import rop.infra.db.jpa.ClaimEntity;
import rop.infra.db.jpa.ClaimJpaRepository;
import rop.spi.ClaimRepository;

import java.util.UUID;

@Repository
public class ClaimRepositoryImpl implements ClaimRepository {

    private final ClaimJpaRepository claimJpaRepository;

    public ClaimRepositoryImpl(ClaimJpaRepository claimJpaRepository) {
        this.claimJpaRepository = claimJpaRepository;
    }

    @Override
    public void createClaim(Claim claim) {
         claimJpaRepository.save(ClaimEntity.fromClaim(claim));
    }

    @Override
    public String nextClaimId() {
        return UUID.randomUUID().toString();
    }
}
