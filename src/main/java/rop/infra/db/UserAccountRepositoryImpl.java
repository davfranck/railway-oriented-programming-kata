package rop.infra.db;

import jakarta.persistence.PersistenceException;
import org.springframework.stereotype.Repository;
import rop.domain.UserAccount;
import rop.domain.exception.NotFoundException;
import rop.domain.exception.TechnicalException;
import rop.infra.db.jpa.UserAccountEntity;
import rop.infra.db.jpa.UserAccountJpaRepository;
import rop.spi.spi.UserAccountRepository;

@Repository
public class UserAccountRepositoryImpl implements UserAccountRepository {

    private final UserAccountJpaRepository userAccountJpaRepository;

    public UserAccountRepositoryImpl(UserAccountJpaRepository userAccountJpaRepository) {
        this.userAccountJpaRepository = userAccountJpaRepository;
    }

    @Override
    public UserAccount findUserAccount(String username) {
        if (username.equals("db_error")) {
            throw new PersistenceException("Db error");
        }
        return userAccountJpaRepository.findByUsername(username)
                .map(UserAccountEntity::toDomain)
                .orElseThrow(() -> new NotFoundException("username " +  username + " not found"));
    }
}
