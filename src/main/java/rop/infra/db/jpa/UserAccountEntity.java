package rop.infra.db.jpa;

import jakarta.persistence.*;
import rop.domain.AccountStatus;
import rop.domain.UserAccount;

@Entity
@Table(name = "USER_ACCOUNT")
public class UserAccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String username;

    @Enumerated(EnumType.STRING)
    private AccountStatus status;

    public UserAccountEntity() {
    }

    public UserAccount toDomain() {
        return new UserAccount(username, status);
    }
}
