package rop.infra.db.jpa;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import rop.domain.Claim;
import rop.domain.PoliceReportStatus;
import rop.domain.Reason;
import rop.domain.Status;

@Entity
@Table(name = "CLAIM")
public class ClaimEntity {
    @Id
    private String id;
    private Status status;
    private String name;
    private Reason reason;
    private PoliceReportStatus policeReportStatus;

    public ClaimEntity() {
    }

    public ClaimEntity(String id, Status status, String name, Reason reason, PoliceReportStatus policeReportStatus) {
        this.id = id;
        this.status = status;
        this.name = name;
        this.reason = reason;
        this.policeReportStatus = policeReportStatus;
    }

    public static ClaimEntity fromClaim(Claim claim) {
        return new ClaimEntity(claim.id(), claim.status(), claim.name(), claim.reason(), claim.policeReportStatus());
    }
}
