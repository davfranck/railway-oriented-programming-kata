package rop.infra.db.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ClaimJpaRepository extends JpaRepository<ClaimEntity, Long> {
}
