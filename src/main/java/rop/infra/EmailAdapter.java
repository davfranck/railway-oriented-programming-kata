package rop.infra;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import rop.domain.Claim;
import rop.domain.exception.TechnicalException;

@Component
public class EmailAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailAdapter.class);

    public void sendClaimCreatedEmail(Claim claim) {
        if (claim.name().equals("email_ko")) {
            throw new TechnicalException("Email not sent");
        }
        LOGGER.info("email sent");
    }
}
