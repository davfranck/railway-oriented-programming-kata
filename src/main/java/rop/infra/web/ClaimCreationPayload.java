package rop.infra.web;

import rop.domain.Reason;
import rop.domain.PoliceReportStatus;

public record ClaimCreationPayload(String name, Reason reason, PoliceReportStatus policeReportStatus) {

}
