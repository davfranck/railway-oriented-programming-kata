package rop.infra.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import rop.domain.Claim;
import rop.domain.ClaimCreationCommand;
import rop.domain.ClaimService;

@RestController
public class ClaimController {

    @Autowired
    private ClaimService claimService;

    @PostMapping("/claim")
    public ResponseEntity<ClaimDto> createInsuranceRecord(@RequestBody ClaimCreationPayload payload) {
        Claim claim = claimService.createInsuranceRecord(new ClaimCreationCommand(payload.name(), payload.reason(), payload.policeReportStatus()));
        ClaimDto claimDto = ClaimDto.fromEntity(claim);
        return ResponseEntity.created(ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{id}").buildAndExpand(claimDto.id()).toUri()).body(claimDto);
    }
}

