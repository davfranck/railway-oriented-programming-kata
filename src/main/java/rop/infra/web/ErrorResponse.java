package rop.infra.web;

public record ErrorResponse(String message) {
}

