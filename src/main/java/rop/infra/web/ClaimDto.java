package rop.infra.web;

import rop.domain.Claim;
import rop.domain.Reason;
import rop.domain.PoliceReportStatus;
import rop.domain.Status;

public record ClaimDto(String id, Status status, String name, Reason reason, PoliceReportStatus policeReportStatus) {
    public static ClaimDto fromEntity(Claim claim) {
        return new ClaimDto(claim.id(), claim.status(), claim.name(), claim.reason(), claim.policeReportStatus());
    }
}
