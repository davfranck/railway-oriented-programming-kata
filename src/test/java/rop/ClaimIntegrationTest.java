package rop;


import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
public class ClaimIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void when_create_claim() throws Exception {
        String jsonPayload = """
                { "name": "bob", "reason" : "FLOOD", "policeReportStatus" : "NO_REPORT" }
                """;
        String jsonContent = """
                { "status" : "IN_PROGRESS", "name": "bob", "reason" : "FLOOD", "policeReportStatus" : "NO_REPORT" }
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(jsonContent));
    }
    @Test
    void when_create_claim_and_user_is_unknown() throws Exception {
        String jsonPayload = """
                { "name": "unknown", "reason" : "BURGLARY", "policeReportStatus" : "REPORTED" }
                """;
        String expectedJson = """
                { "message": "username unknown not found" }
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isNotFound())
                .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }
    @Test
    void when_create_claim_and_claim_is_invalid() throws Exception {
        String jsonPayload = """
                { "name": "invalid_claim", "reason" : "BURGLARY", "policeReportStatus" : "NO_REPORT" }
                """;
        String expectedJson = """
                { "status" : "REFUSED", "name": "invalid_claim", "reason" : "BURGLARY", "policeReportStatus" : "NO_REPORT" }
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }
    @Test
    void when_create_claim_and_user_account_is_closed() throws Exception {
        String jsonPayload = """
                { "name": "closed", "reason" : "BURGLARY", "policeReportStatus" : "REPORTED" }
                """;
        String expectedJson = """
                { "message": "User account is closed" }
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isBadRequest())
                .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }
    @Test
    void when_create_claim_and_email_not_sent() throws Exception {
        String jsonPayload = """
                { "name": "email_ko", "reason" : "BURGLARY", "policeReportStatus" : "REPORTED" }
                """;
        String expectedJson = """
                { "status" : "IN_PROGRESS", "name": "email_ko", "reason" : "BURGLARY", "policeReportStatus" : "REPORTED"}
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }
    @Test
    void when_create_claim_and_technical_error_in_database() throws Exception {
        String jsonPayload = """
                { "name": "db_error", "reason" : "BURGLARY", "policeReportStatus" : "REPORTED" }
                """;
        String expectedJson = """
                { "message" : "Db error" }
                """;
        mockMvc.perform(post("/claim").contentType(MediaType.APPLICATION_JSON).content(jsonPayload))
                .andExpect(status().isInternalServerError())
                .andExpect(MockMvcResultMatchers.content().json(expectedJson));
    }
}
